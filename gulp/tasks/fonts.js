'use strict';

var gulp = require('gulp'),
	outputDir = 'build';

gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
  .pipe(gulp.dest(outputDir + '/fonts'))
});