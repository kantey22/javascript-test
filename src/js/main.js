//Scroll function
 $(document).ready(function() {
   $(document).on("scroll", onScroll);

   $('a[href^="#"]').on('click', function(e) {
     e.preventDefault();
     $(document).off("scroll");
     var target = this.hash;
     $target = $(target);
     $('html, body').stop().animate({
       'scrollTop': $target.offset().top + 0
     }, 500, 'swing', function() {
       window.location.hash = target;
       $(document).on("scroll", onScroll);
     });
   });
 });

 function onScroll(event) {
   var scrollPosition = $(document).scrollTop();
   $(' a').each(function() {
     var currentLink = $(this);
     var refElement = $(currentLink.attr("href"));
   });
 }

 $(window).scroll(function() {
   if ($(this).scrollTop() >= 100) { // If page is scrolled more than 50px
     $('#return-to-top').fadeIn(200); // Fade in the arrow
   } else {
     $('#return-to-top').fadeOut(200); // Else fade out the arrow
   }
 });

 //return to top function
 $('#return-to-top').click(function() { // When arrow is clicked
   $('body,html').animate({
     scrollTop: 0 // Scroll to top of body
   }, 500);
 });

 //Check Age And Height Exercise
 function checkAge(age) {

   var message = "";
   if (age >= 0 && age <= 12) {
     message = "Child"

   } else if (age >= 13 && age <= 18) {
     message = "Teenager"
   } else if (age >= 19 && age <= 35) {
     message = "Adult"
   } else if (age >= 36) {
     message = "Senior"
   } else {
     message = " ";
   }

   return message;
 }

 function checkHeight(height) {
   var message = "";
   if (height <= 5.2) {
     message = "short"
   } else if (height >= 5.3 && height <= 5.7) {
     message = "Average"
   } else if (height >= 5.8) {
     message = "Tall"
   } else {
     message = "";
   }

   return message;
 }

 function checkValues() {
   var age = parseInt(document.getElementById("age").value);
   var height = document.getElementById("height").value;
   message = checkAge(age).bold() + " And your height is " + checkHeight(height).bold();
   document.getElementById("demo").innerHTML = message;
 }

document.getElementById("btnSubmit").onclick = function(e){e.preventDefault(); checkValues()};

 // Find Intersection of two arrays

 var firstArray = [2, 2, 2, 4, 1];
 var secondArray = [1, 2, 0, 2];

 function findIntersection(arr1, arr2) {
   var t;
   if (arr2.length > arr1.length)
     t = arr2,
     arr2 = arr1,
     arr1 = t;
   return arr1.filter(function(e) {
     return arr2.indexOf(e) > -1;
   }).filter(function(e,i,c)
 {
   return c.indexOf(e)===i;
 });


 }

 var intersection = findIntersection(firstArray, secondArray);
 document.getElementById("intersection_output").innerHTML = intersection;

 // String Transformer Functions

function checkString()
{
   var stringTransformer = document.getElementById("string_transfromer").value;
   var newString = stringTransformer.replace(/(g)|(c)|(t)|(a)/gi,function(str,paramG,paramC,paramT,paramA) {
           if(paramG) return 'C';
           if(paramC) return 'G';
           if(paramT) return 'A';
           if(paramA) return 'U';
       });

document.getElementById("string_output").innerHTML = newString;

}
document.getElementById("btnString").onclick = function(e){e.preventDefault(); checkString()};
